import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# read tex.din into dataframe
df1 = pd.read_table('./data/tex.din', sep=' ', header=None)
df1.columns = ['operation', 'address']
# convert hexadecimal byte address to decimal number expressed in mil
df1['address'] = sorted(df1['address'].apply(lambda x: int(x, 16))/1000000)

# read cc1.din into dataframe
df2 = pd.read_table('./data/cc1.din', sep=' ', header=None)
df2.columns = ['operation', 'address']
# convert hexadecimal byte address to decimal number expressed in mil
df2['address'] = sorted(df2['address'].apply(lambda x: int(x, 16))/1000000)

# draw the histograms for address distribution
fig = plt.figure(figsize=(12, 4))
pic1 = fig.add_subplot(121)
plt.hist(df1['address'])
plt.title('Histogram of address distribution from tex.din')
plt.xlabel('Address(million)')
plt.ylabel('Number of occurrences')
pic2 = fig.add_subplot(122)
plt.hist(df2['address'])
plt.title('Histogram of address distribution from cc1.din')
plt.xlabel('Address(million)')
plt.ylabel('Number of occurrences')
plt.show()
print('0: read data; 1: write data; 2: instruction fetch')
print('Frequency of operation the CPU performs from text.din:')
df1_freq = df1.groupby(['operation']).size().to_frame('freq').reset_index()
df1_freq['Relative Frequency'] = df1_freq['freq'] / df1['address'].count()
print(df1_freq)
print('Frequency of operation the CPU performs from cc1.din:')
df2_freq = df2.groupby(['operation']).size().to_frame('freq').reset_index()
df2_freq['Relative Frequency'] = df2_freq['freq'] / df2['address'].count()
print(df2_freq)

# Further analysis on r/w frequency and address distribution
df1 = df1.groupby(['operation', 'address']).size().to_frame('freq').reset_index()
df1 = df1.sort_values(['freq'], ascending=False)
df1 = df1[1:1000]
pic1X0 = np.array(df1[df1['operation'] == 0]['address'])
pic1Y0 = np.array(df1[df1['operation'] == 0]['freq'])
pic1X1 = np.array(df1[df1['operation'] == 1]['address'])
pic1Y1 = np.array(df1[df1['operation'] == 1]['freq'])
pic1X2 = np.array(df1[df1['operation'] == 2]['address'])
pic1Y2 = np.array(df1[df1['operation'] == 2]['freq'])

df2 = df2.groupby(['operation', 'address']).size().to_frame('freq').reset_index()
df2 = df2.sort_values(['freq'], ascending=False)
df2 = df2[1:1000]
pic2X0 = np.array(df2[df2['operation'] == 0]['address'])
pic2Y0 = np.array(df2[df2['operation'] == 0]['freq'])
pic2X1 = np.array(df2[df2['operation'] == 1]['address'])
pic2Y1 = np.array(df2[df2['operation'] == 1]['freq'])
pic2X2 = np.array(df2[df2['operation'] == 2]['address'])
pic2Y2 = np.array(df2[df2['operation'] == 2]['freq'])

# draw the histograms for address distribution by CPU operation
fig = plt.figure(figsize=(14, 4))
pic1 = fig.add_subplot(121)
plt.bar(pic1X0, height=pic1Y0, width=50, linewidth=1, edgecolor='w', color='b', alpha=0.8, label='read data')
plt.bar(pic1X1 + 50, height=pic1Y1, width=50, linewidth=1, edgecolor='w', color='y', alpha=0.8, label='write data')
plt.bar(pic1X2 + 100, height=pic1Y2, width=50, linewidth=1, edgecolor='w', color='r', alpha=0.8, label='instruction fetch')
plt.legend()
plt.title('Histogram of address distribution by CPU operation from tex.din')
plt.xlabel('Address(million)')
plt.ylabel('Number of occurrences')
pic2 = fig.add_subplot(122)
plt.bar(pic2X0, height=pic2Y0, width=50, linewidth=1, edgecolor='w', color='b', alpha=0.8, label='read data')
plt.bar(pic2X1 + 50, height=pic2Y1, width=50, linewidth=1, edgecolor='w', color='y', alpha=0.8, label='write data')
plt.bar(pic2X2 + 100, height=pic2Y2, width=50, linewidth=1, edgecolor='w', color='r', alpha=0.8, label='instruction fetch')
plt.legend()
plt.title('Histogram of address distribution by CPU operation from cc1.din')
plt.xlabel('Address(million)')
plt.ylabel('Number of occurrences')
plt.show()
print(df1[(df1['freq'] > 10) & (df1['operation'] < 2)])
print(pic1X0)
print(df2[(df2['freq'] > 10) & (df2['operation'] < 2)])