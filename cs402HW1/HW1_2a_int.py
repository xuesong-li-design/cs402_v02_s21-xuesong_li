import numpy as np
import time


def create_matrix(n_row, n_col):
    # Using random integers between 1 and 1000
    matrix = np.random.randint(1, 1000, size=(n_row, n_col))
    return matrix


def matrix_multiply(x, y):
    # size of matrix i x j
    z = [[0 for _ in range(len(x))] for _ in range(len(y[0]))]
    for i in range(len(x)):
        for j in range(len(y[0])):
            for k in range(len(y)):
                z[i][j] += x[i][k]*y[k][j]
    return z


if __name__ == '__main__':
    begin_time = time.perf_counter()
    matrix1 = create_matrix(300, 500)
    matrix2 = create_matrix(500, 300)

    # matrix1 = [[1,2,3],[4,5,6]]
    # matrix2 = [[10,11],[20,21],[30,31]]
    # new_matrix after multiplication should be [[140, 146], [320, 335]]
    new_matrix = matrix_multiply(matrix1, matrix2)
    end_time = time.perf_counter()
    print('Running time: %.02f Seconds' % (end_time - begin_time))
    # print(matrix1)
    # print(matrix2)
    # print(new_matrix)
